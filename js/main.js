var monthLabels=["Aug 1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","Sep 1","2","3"];

var dataUsage=['115', '118', '124', '130', '145', '143', '158', '112', '130', '180', '205', '218', '243', '207', '203', '201', '173', '205', '216', '203', '218', '233', '196', '201', '185', '167', '175', '185', '183', '180', '181', '180', '181', '183'];
var dataTempMin=['80', '77', '115', '83', '122', '110', '117', '90', '110', '155', '160', '183', '223', '175', '158', '143', '145', '177', '180', '176', '200', '202', '163', '164', '148', '132', '134', '152', '162', '185', '183', '161', '160', '183'];
var dataTempMax=['140', '150', '158', '160', '165', '170', '176', '145', '167', '215', '230', '242', '260', '230', '228', '225', '203', '238', '240', '230', '258', '260', '221', '224', '208', '202', '208', '210', '218', '215', '216', '218', '217', '216'];
var ctx1 = document.getElementById('chart__usage');
var chart1 = new Chart(ctx1, {
    type: 'bar',
    data: {
        labels: monthLabels,
        datasets: [{
            label: 'Usage',
            type: 'bar',
            yAxisID: 'A',
            data: dataUsage,
            borderColor:'#F0F0F0',
            highlightFill: "rgba(30,30,30,0.55)"
        }, {
            label: 'Low Temp',
            type: "line",
            yAxisID: 'A',
            data: dataTempMin,
            borderColor:'#3BA1B5',
            pointBackgroundColor: "#3BA1B5",
            backgroundColor:'rgba(0,0,0,0)'
        }, {
            label: 'High Temp',
            type: "line",
            yAxisID: 'A',
            data: dataTempMax,
            borderColor:'#EF5B4E',
            pointBackgroundColor: "#EF5B4E",
            backgroundColor:'rgba(0,0,0,0)'
        }]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            xAxes:[{
                barPercentage:0.4,
                ticks: {
                    autoSkip: false,
                    fontColor:'black',
                    fontSize: 14,
                    maxRotation: 90,
                    minRotation: 90
                }
            }],
            yAxes: [{
                id: 'A',
                type: 'linear',
                ticks: {
                    stepSize: 20,
                    fontColor:'black',
                    fontSize:14,
                    max: 260,
                    min: 0
                },
                scaleLabel: {
                    display: true,
                    fontColor:'black',
                    fontSize: 14,
                    position: 'top',
                    color: 'black',
                    labelString: 'Usage (kWh)'
                }
            }, {
                id: 'B',
                type: 'linear',
                position: 'right',
                ticks: {
                    fontColor:'black',
                    fontSize:14,
                    stepSize: 10,
                    max: 120,
                    min: -10
                },
                scaleLabel: {
                    fontColor:'black',
                    fontSize:14,
                    display: true,
                    position: 'top',
                    color: 'black',
                    labelString: 'Temperature (\u00B0F)'
                },
                afterTickToLabelConversion : function(q){
                    for(var tick in q.ticks){
                        q.ticks[tick] += '\u00B0';
                    }
                }
            }],
            gridLines:{
                color: 'rgba(0,0,0,0.1)',
                zeroLineWidth: 8
            }
        },
        elements: {
            line: {
                borderWidth: 1,
                tension: 0 // disables bezier curves
            },
            point:{
                borderWidth: 4,
            }
        },
        legend:{
            display: false,
        },
        legendCallback: function(chart) {
            console.log('html legend working')
            var text = [];
            text.push('<ul class="legend">');
            for (var i = 0; i < chart.data.datasets.length; i++) {
                text.push('<li><span style="background-color:' +
                    chart.data.datasets[i].borderColor +
                    '"></span>');
                if (chart.data.datasets[i].label) {
                    text.push(chart.data.datasets[i].label);
                }
                text.push('</li>');
            }
            text.push('</ul>');
            return text.join('');
        }
    }
});

var dataMktAvg=['12', '8', '10', '6', '9', '10', '4', '10', '4', '5', '11', '3', '2', '12', '4', '9', '8', '4', '4', '11', '2', '3', '5', '10', '4', '9', '5', '10', '2', '12', '5', '8', '7', '3'];
var dataUserAvg=['13', '10', '10', '10', '10', '13', '8', '9', '9', '12', '8', '4', '9', '5', '3', '10', '5', '13', '10', '4', '9', '11', '3', '8', '10', '10', '13', '8', '3', '13', '10', '11', '2', '9'];
var ctx2 = document.getElementById('chart__energy');
var chart2 = new Chart(ctx2, {
    type: 'line',
    data: {
        labels: monthLabels,
        datasets: [{
            label: 'Market Average',
            yAxisID: 'A',
            data: dataMktAvg,
            backgroundColor: '#EF5B4E',
            pointBackgroundColor: "#EF5B4E",
            borderColor: '#EF5B4E',
            fill: false  //no fill here
        }, {
            label: 'Your Average',
            data: dataUserAvg,
            backgroundColor: '#3BA1B5',
            pointBackgroundColor: "#242435",
            borderColor: '#242435',
            fill: '-1' //fill until previous dataset
        }, {
            label: 'Your Savings',
            data: dataUserAvg,
            backgroundColor: '#3BA1B5',
            borderColor: '#3BA1B5',
            fill: '-1' //fill until previous dataset
        }]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            xAxes:[{
                ticks: {
                    autoSkip: false,
                    fontColor: 'black',
                    fontSize: 14,
                    maxRotation: 90,
                    minRotation: 90
                }
            }],
            yAxes: [{
                id: 'A',
                type: 'linear',
                ticks: {
                    stepSize: 1,
                    fontColor: 'black',
                    fontSize: 14,
                    max: 15,
                    min: 0
                },
                scaleLabel: {
                    fontColor: 'black',
                    fontSize: 14,
                }
            }],
            gridLines:{
                color: '#F0F0F0',
                zeroLineWidth: 8
            }
        },
        elements: {
            line: {
                borderWidth: 1,
                tension: 0 // disables bezier curves
            },
            point:{
                borderWidth: 4,
            }
        },
        legend:{
            display:false
        },
        legendCallback: function(chart) {
            console.log('html legend2 working');
            console.log(chart);
            var text = [];
            text.push('<ul class="legend">');
            for (var i = 0; i < chart.data.datasets.length; i++) {
                text.push('<li><span style="background-color:' +
                    chart.data.datasets[i].borderColor +
                    '"></span>');
                if (chart.data.datasets[i].label) {
                    text.push(chart.data.datasets[i].label);
                }
                text.push('</li>');
            }
            text.push('</ul>');
            return text.join('');
        }
    }
});
document.getElementById('chart__energy_legends').innerHTML = chart2.generateLegend();
document.getElementById('chart__usage_legends').innerHTML = chart1.generateLegend();
